import Vue from 'vue'
import Vuex from 'vuex';
import countModule from './modules/countModule.js';
import uiModule from './modules/uiModule.js';

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
       countModule: countModule,
       uiModule: uiModule
    }
})