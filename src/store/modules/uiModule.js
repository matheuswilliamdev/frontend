
export default{
    namespaced: true,
    state: {
        showResultsPanelMobile: false,
        floatingActionBtnMobileIcon: 'mdi-arrow-right'
    },
    mutations:{
       CHANGE_SHOW_PANELS_MOBILE: (state) => {
            state.showResultsPanelMobile = !state.showResultsPanelMobile;
            state.floatingActionBtnMobileIcon = state.showResultsPanelMobile ? 'mdi-arrow-left' : 'mdi-arrow-right';
       },
    }
}