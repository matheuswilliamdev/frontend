## Implementação Avaliação Técnica - Front-end Convenia

  

### Descrição

  

Implementação da avaliação técnica - Front-end realizado pela Convenia para a vaga de Desenvolvedor Front-end.

O deploy desta aplicação encontra-se disponível do link: 
 https://desafio-convenia-kaledo.web.app/
  

#### Tecnologias e Bibliotecas utilizadas

- NodeJs

- VueJs

- Vuex

- Vuetify

- Firebase Tools (CLI)

  

#### Como executar o projeto

  

Pré-requisitos: NodeJs (v 14.15.4)

  

1 - Faça o clone do repositório do projeto:

```bash

git clone https://gitlab.com/matheuswilliamdev/frontend.git

```

  

2 - Após clonado o repositório, acesse o diretório do projeto:

```bash

cd frontend

```

3 - Dentro do repositório do projeto, antes de rodar o comando **```npm install```**, é necessário copiar o arquivo ```.env.example``` e renomeá-lo para ```.env``` e preencher a variável ```VUE_APP_EXCHANGE_RATE_API_KEY```.

  

4 - Após configurado o arquivo **.env** você irá rodar o comando do npm para instalar todas as biliotecas utilizadas no projeto:

  

```bash

npm install

```

  

5 -  Após a instalação das bibliotecas, você deverá executar o comando:
```bash

npm run serve

```


Após o comando **``` npm run serve```** ser executado o projeto estará rodando localmente, para visualizá-lo acesse a url abaixo:

  

```bash

http://localhost:8080/

```

### Autor

Matheus William Gomes dos Santos

  

https://www.linkedin.com/in/matheus-william-6a10a1185/